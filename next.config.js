const path = require('path')

module.exports = {
  webpack: function (config) {
    // Setup aliases
    config.resolve.alias = {
      ...config.resolve.alias,
      '~': path.resolve(__dirname)
    }

    return config
  }
}

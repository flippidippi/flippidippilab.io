import Head from 'next/head'
import { useRouter } from 'next/router'

function Title () {
  const { query } = useRouter()

  function handleTitleChange (event: React.ChangeEvent<HTMLInputElement>) {
    document.title = event.target.value
  }

  return (
    <>
      <Head>
        <title>{query['title'] ?? ''}</title>
      </Head>

      <input type='text' defaultValue={query['title'] ?? ''} onChange={handleTitleChange} />
      <style jsx>{`
        input {
          display: block;
          font-size: 2rem;
          margin: 50px auto;
          min-width: 1000px;
          padding: 1rem;
          text-align: center;
        }
      `}
      </style>
    </>
  )
}

export default Title

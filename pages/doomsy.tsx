import Head from 'next/head'

function Doomsy () {
  return (
    <div className='root'>
      <Head>
        <title>doomsy</title>
      </Head>

      <div className='horizontal'>
        <div className='vertical'>
          <img src='/static/img/bread.png' alt='bread' />
        </div>
      </div>

      <style jsx global>{`
        html, body, #__next {
          height: 100%;
          margin: 0;
          padding: 0;
          overflow: hidden;
        }
      `}
      </style>

      <style jsx>{`
        .root {
          height: 100%;
        }

        .horizontal {
          align-items: center;
          display: flex;
          justify-content: center;
          margin: 0;
          min-height: 100%;
          padding: 0;
          width: 100%;
        }

        .vertical {
          display: flex;
          flex-direction: column;
          height: 100%;
          justify-content: center;
        }

        img {
          transition: transform 10s;
          width: 50px;
        }

        img:hover {
          transform: scale(100);
        }
      `}
      </style>
    </div>
  )
}

export default Doomsy

import Head from 'next/head'
import { useState } from 'react'

function Titles () {
  const [titles, setTitles] = useState('')

  function handleTitleChange (event: React.ChangeEvent<HTMLInputElement>) {
    setTitles(event.target.value)
  }

  function handleTitleSubmit (event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault()

    for (const title of titles.split(' ')) {
      window.open(`/title?title=${title}`)
    }
  }

  return (
    <>
      <Head>
        <title>TBD</title>
      </Head>

      <form onSubmit={handleTitleSubmit}>
        <input type='text' value={titles} onChange={handleTitleChange} />
      </form>
      <style jsx>{`
        input {
          display: block;
          font-size: 2rem;
          margin: 50px auto;
          min-width: 1000px;
          padding: 1rem;
          text-align: center;
        }
      `}
      </style>
    </>
  )
}

export default Titles

import Head from 'next/head'

function Tugboat () {
  return (
    <div>
      <Head>
        <title>tugboat</title>
      </Head>

      <img src='/static/img/tugboat.jpeg' />
    </div>
  )
}

export default Tugboat

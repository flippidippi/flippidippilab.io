function Index () {
  return (
    <>
      <div className='row'>
        <div className='col outer'>
          <div className='row'>
            <div className='col'>
              <img className='face' src='/static/img/face.png' />
            </div>

            <div className='col title-col'>
              <div className='title'>
                flippidippi
              </div>
              <div className='subtitle'>
                I probably like you and tell everyone you're awesome.
              </div>
            </div>
          </div>

          <div className='row social'>
            <div className='col'>
              <a href='https://www.instagram.com/flippidippi_' target='_blank' rel='noopener noreferrer'>instagram</a>
            </div>
            <div className='col'>
              <a href='https://www.linkedin.com/in/flippidippi/' target='_blank' rel='noopener noreferrer'>linkedin</a>
            </div>
            <div className='col'>
              <a href='https://gitlab.com/flippidippi' target='_blank' rel='noopener noreferrer'>gitlab</a>
            </div>
            <div className='col'>
              <a href='https://github.com/flippidippi' target='_blank' rel='noopener noreferrer'>github</a>
            </div>
            <div className='col'>
              <a href='https://www.npmjs.com/~flippidippi' target='_blank' rel='noopener noreferrer'>npm</a>
            </div>
            <div className='col'>
              <a href='https://www.paypal.me/flippidippi' target='_blank' rel='noopener noreferrer'>paypal</a>
            </div>
          </div>

          <div>
            <iframe
              allow='autoplay *; encrypted-media *; fullscreen *; clipboard-write'
              sandbox='allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation'
              src='https://embed.music.apple.com/us/playlist/heart/pl.u-ZmYNGT01E3Do?theme=dark'
            ></iframe>
          </div>
        </div>
      </div>

      <style jsx global>{`
        html, body, #__next {
          background-attachment: fixed;
          background-image: var(--background-image);
          background-repeat: no-repeat;
          background-size: cover;
          height: 100%;
          margin: 0;
          padding: 0;
        }
      `}
      </style>

      <style jsx>{`
        .row {
          align-items: center;
          display: flex;
          flex-wrap: wrap;
          justify-content: center;
          margin: 0;
          min-height: 100%;
          padding: 0;
          width: 100%;
        }

        iframe {
          padding: 20px 0;
          height: 500px;
          width: 100%;
          border: 0;
        }

        .col {
          text-align: center;
          max-width: 100%;
          align-self: center;
        }

        .outer {
          margin: 0 15px;
        }

        .face {
          max-width: 200px;
          padding: 0 15px;
          width: auto;
        }

        .title-col {
          padding: 20px;
          margin-top: 10px;
          background: var(--color-dark);
          border-radius: 10px;
        }

        .title {
          color: var(--title-color);
          font-size: 60px;
          font-weight: 700;
        }

        .subtitle {
          font-weight: 300;
        }

        .social {
          background: var(--color-dark);
          border-radius: 10px;
          margin: 20px 0 0 0;
          padding: 15px 0;
        }

        .social div {
          padding: 0 15px;
        }

        .social a {
          display: inline;
          font-size: 20px;
        }
      `}
      </style>
    </>
  )
}

export default Index

import { useRouter } from 'next/router'

function ResponsiveImage () {
  const { query } = useRouter()

  return (
    <>
      <div>
        <img src={query.src?.toString()} />
      </div>

      <style jsx>{`
        div {
          background-color: #${query.color ?? '000000'};
          height: 100%;
          text-align: center;
          width: 100%;
        }

        div img {
          max-height: 100%;
          max-width: 100%;
        }
      `}</style>
    </>
  )
}

export default ResponsiveImage
